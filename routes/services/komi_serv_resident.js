const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
router.use(checkAuth);

const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const idTypeData = await conn
      .where("id_tenant", dcodeInfo.idtenant)
      .select("*")
      .from("m_resident");
    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { resident: idTypeData } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let id = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')

    const idTypeData = await conn
      .where("id", id)
      .select("*")
      .from("m_resident");

    if (idTypeData.length > 0) {
      setTimeout(function () {
        res
          .status(200)
          .json({ status: 200, data: { resident: idTypeData[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { code, name, status_cb, status_bf, status } = req.body;

    const resp = await conn("m_resident")
      .returning([
        "code",
        "name",
        "status_cb",
        "status_bf",
        "status",
        "id_tenant",
      ])
      .insert({
        code: code,
        name: name,
        status_cb: status_cb,
        status_bf: status_bf,
        status_bf: status_bf,
        status: status,
        id_tenant: dcodeInfo.idtenant,
      });

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { id, code, name, status_cb, status_bf, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("m_resident")
      .returning(["id", "code", "name", "status_cb", "status_bf", "status"])
      .where("id", id)
      .update({
        code: code,
        name: name,
        status_cb: status_cb,
        status_bf: status_bf,
        status: status,
      });

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/delete/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let id = req.params.id;
  console.log("### Bank Code ### " + id);
  try {
    let resp = await conn("m_resident").where("id", id).del();

    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
