const express = require("express"),
    app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");


var conf = require("../../config.json");
const moment = require("moment");
const {duration} = require("moment-timezone");
const bcrypt = require('bcrypt');
const conn = knex.conn();
router.use(checkAuth);

router.get("/getAll", async (req, res, next) => {
    try {

        const data = await conn.select("*").from("kc_channel");
        console.log(data)
        if (data.length > 0) {
            setTimeout(function () {
                res
                    .status(200)
                    .json({status:200, data: { channel : data }});
            }, 500);
        } else {
            setTimeout(function () {
                res.status(200).json({
                    status: 202,
                    data: "Data tidak ditemukan"
                });
            }, 500);
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({
            status: 500,
            data: "Internal Error"
        });
    }
});

router.get("/getById/:channel_id", async (req, res, next) => {
    
    try {
     let channel  = req.params.channel_id;
     console.log(channel);

     const conChannelData = await conn
         .select('*')
         .from('kc_channel')
         .where('channel_id', channel);

        console.log(conChannelData)

     if (conChannelData.length > 0) {
             res.status(200).json({ status: 200, data: { channel: conChannelData[0] } });
     } else {
             res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
     }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
});

router.post("/insert", async (req, res, next) => {
    var dcodeInfo = req.userData;
    
    try {
        const { channelId, channelName, channelType, merchantCode, secretKey} = req.body;
        const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const password = bcrypt.hashSync(secretKey, 4);

        const resp = await conn("kc_channel")
            .insert({
                channel_id: channelId,
                channel_name: channelName,
                channel_type: channelType,
                merchant_code: merchantCode,
                secret_key: password,
                modif_dt: today,
                create_dt: today
            })
            .returning([
                "channel_id",
                "channel_name",
                "channel_type",
                "merchant_code",
                "create_dt",
                "modif_dt"
            ]);

        if (resp.length > 0) {
            res.status(201).json({
                status: 200,
                data: "Data inserted"
            });
        } else {
            res.status(500).json({
                status: 500,
                data: "Error to insert",
            });
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error insert Data" });
    }
});

router.post("/update", async (req, res, next) => {
    try {
        const { channelId, channelName, channelType, merchantCode, secretKey, daily_limit_amount } = req.body;
        const password = bcrypt.hashSync(secretKey, 4);

        const resp = await conn ("kc_channel")
            .where("channel_id", channelId)
            .update({
                channel_name: channelName,
                channel_type: channelType,
                merchant_code: merchantCode,
                secret_key: password,
                daily_limit_amount: daily_limit_amount
            })

        // crypt($1,gen_salt('bf',4))

        if (resp > 0) {
            res.status(200).json({
                status: 200, data: "Data has been update" });
        } else {
            res.status(500).json({
                status: 500,
                data: "Error update data ",
            });
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error insert data" });
    }
});

router.post("/getByParam", async (req, res, next) => {
    try {
        const { channelId, channelName } = req.body;

        const getByparam = await conn ("kc_channel")
            .select("*")
            .modify(function (queryBuilder){
                if (channelId) {
                    queryBuilder.where("channel_id", channelId);
                }
                if (channelName) {
                    queryBuilder.where("channel_name", channelName);
                }
            });
        if (getByparam.length > 0) {
            res.status(200).json({ status: 200, data: getByparam });
        } else {
            res.status(200).json({
                status: 202,
                data: "No Content",
            });
        }
        
    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
});

router.delete("/delete/:channel_id", async (req, res, next) => {

    let channelId = req.params.channel_id;
    console.log(channelId);

    try {
        let resp = await conn("kc_channel")
            .where("channel_id", channelId).del();

        res.status(200).json({ status: 200, data: "Success" });

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
});

router.get("/getChannel", async (req, res, next) => {
    try {
        const data = await conn.select("channel_type","channel_name")
            .from("kc_channel")

        console.log(data)

        if (data.length > 0) {
            setTimeout(function () {
                res
                    .status(200)
                    .json({status:200, data});
            }, 500);
        } else {
            setTimeout(function () {
                res.status(200).json({
                    status: 202,
                    data: "Data tidak ditemukan"
                });
            }, 500);
        }
    } catch (e) {

    }
});

module.exports = router;
