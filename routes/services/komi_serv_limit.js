const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const idtenant = dcodeInfo.idtenant;
  // const idtenant = 1;
  try {
    // var query =
    //   "SELECT  *, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') formated_date FROM m_limit WHERE id_tenant=$1 order by created_date desc";

    // const limitData = await pool.query(query, [dcodeInfo.idtenant]);

    const limitData = await conn
      .select("*")
      .from("m_limit")
      .where("idtenant", idtenant)
      .orderBy("created_date", "desc");

    if (limitData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { limit: limitData } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let limitId = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT  *, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') formated_date FROM m_limit WHERE id=$1  order by created_date desc";
    // const limitData = await pool.query(query, [limitId]);
    const limitData = await conn
      .select("*")
      .from("m_limit")
      .where("id", limitId)
      .orderBy("created_date", "desc");

    if (limitData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { limit: limitData[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const idtenant = dcodeInfo.idtenant;
  // const idtenant = 1;
  try {
    // var apps = dcodeInfo.apps[0];
    const { limit_name, tran_type, min_tran, max_limit, status } = req.body;
    // const resp = await pool.query(
    //   "INSERT INTO m_limit (limit_name, tran_type, min_tran, max_limit, status,id_tenant) VALUES ($1, $2, $3, $4, $5,$6) ",
    //   [limit_name, tran_type, min_tran, max_limit, status, dcodeInfo.idtenant]
    // );

    const resp = await conn("m_limit")
      .insert({
        limit_name: limit_name,
        tran_type: tran_type,
        min_tran: min_tran,
        max_limit: max_limit,
        status: status,
        idtenant: idtenant,
      })
      .returning(["id"]);

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var apps = dcodeInfo.apps[0];
    const { id, limit_name, tran_type, min_tran, max_limit, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE m_limit SET limit_name=$1, tran_type=$2, min_tran=$3, max_limit=$4, status=$5 WHERE id=$6 ",
    //   [limit_name, tran_type, min_tran, max_limit, status, id]
    // );

    const resp = await conn("m_limit")
        .where("id", id).update({
      limit_name: limit_name,
      tran_type: tran_type,
      min_tran: min_tran,
      max_limit: max_limit,
      status: status,
    });

    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.get("/delete/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let id = req.params.id;
  console.log("### Bank Code ### " + id);
  try {
    // let resp = await pool.query("DELETE FROM m_limit where id = $1", [
    //   id,
    // ]);
    let resp = await conn("m_limit").where("id", id).del();

    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
