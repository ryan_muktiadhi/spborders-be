const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
router.use(checkAuth);

var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllMonTrx", async (req, res, next) => {
  //   const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );
    const today = moment().format("YYYY-MM-DD");
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    //   var query = "SELECT id, paramname, paramvalua, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM public.m_systemparam WHERE idtenant=$1 order by created_date desc";

    // const bicAdmins = await conn("trx_log as tl")
    //   .join("m_channeltype as mct", "tl.channel", "=", "mct.channelcode ")
    //   .select(
    //     "tl.id",
    //     "tl.status",
    //     "tl.trxnumber",
    //     " tl.refnumbifast",
    //     "mct.channeltype as channel",
    //     "tl.destbank",
    //     "tl.srcbank",
    //     "tl.destaccnum",
    //     "tl.srcaccnum",
    //     "tl.destaccname",
    //     " tl.srcaccname",
    //     "tl.created_date",
    //     "tl.change_who",
    //     "tl.last_updated_date",
    //     "tl.amount",
    //     conn.raw(
    //       "CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype"
    //     ),
    //     "tl.idtenant"
    //   )
    //   .where({
    //     "tl.idtenant": dcodeInfo.idtenant,
    //   })
    //   .andWhere("tl.created_date", ">=", today)
    //   .orderBy("tl.created_date", "desc");

    let bicAdmins = [
      {
        id: "4",
        komi_unique_id: "KOMI111111111", // trans id
        bifast_trx_no: "129391239129391", // ref number
        komi_trx_no: "1231232333423", //
        channel_type: "01", // chanel
        branch_code: null,
        trx_amount: 5000,
        recipient_proxy_type: "01",
        recipient_proxy_alias: "08128477654",
      },
      {
        id: "5",
        komi_unique_id: "KOMI123123123", // trans id
        bifast_trx_no: "129391239129391", // ref number
        komi_trx_no: "1231232333423", //
        channel_type: "01", // chanel
        branch_code: null,
        trx_amount: 9000,
        recipient_proxy_type: null,
        recipient_proxy_alias: null,
      },
    ];
    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/getbyparam", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    // {"start_date":"2021-09-24","end_date":"2021-09-26","status":1,"channel":"C002","trxtype":1}
    const { start_date, end_date, status, channel, trxtype } = req.body;
    let queryhelper = "";

    console.log(">> Tambahan >> : " + queryhelper);

    const resp = await conn("trx_log as tl")
      .join("m_channeltype as mct", "tl.channel", "=", "mct.channelcode ")
      .select(
        "tl.id",
        "tl.status",
        "tl.trxnumber",
        " tl.refnumbifast",
        "mct.channeltype as channel",
        "tl.destbank",
        "tl.srcbank",
        "tl.destaccnum",
        "tl.srcaccnum",
        "tl.destaccname",
        " tl.srcaccname",
        "tl.created_date",
        "tl.change_who",
        "tl.last_updated_date",
        "tl.amount",
        conn.raw(
          "CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype"
        ),
        "tl.idtenant"
      )
      .where({
        "tl.idtenant": dcodeInfo.idtenant,
      })
      .modify(function (queryBuilder) {
        if (status) {
          queryBuilder.where("tl.status", status);
        }
        if (channel) {
          queryBuilder.where("mct.channelcode", channel);
        }
        if (trxtype) {
          queryBuilder.where("tl.transacttype", trxtype);
        }
        if (start_date && end_date) {
          queryBuilder.whereBetween("tl.created_date", [start_date, end_date]);
        }
      });

    if (resp.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: resp });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: [],
        });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert m_proxytypes" });
  }
});

module.exports = router;
