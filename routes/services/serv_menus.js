const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");

/*POST END*/

router.get("/switchermenu", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/sidemenu", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/aclpages", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;
