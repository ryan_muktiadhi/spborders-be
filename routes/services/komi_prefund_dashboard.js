const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
const axios = require("axios");
const conn = knex.conn();
const shell = require("shelljs");
var osu = require("node-os-utils");

// router.use(checkAuth);

//getAllGroup
router.get("/getProp", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var query = "SELECT * FROM prefund_dashboard";
    //const data = await pool.query(query, []);
    const data = await conn.select("*").from("prefund_dashboard");
    if (data.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { dashboardProp: data[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        let rsult = { dashboardProp: { min: 0, max: 0, curent: 0 } };

        res.status(200).json({ status: 202, data: rsult });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getHistory", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let id = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT  *, TO_CHAR(datetime,'yyyy-MM-dd HH:mm:ss') formated_date FROM prefund_history  order by datetime desc";
    // const accTypeData = await pool.query(query, []);

    const accTypeData = await conn.select("*").from("prefund_history");
    if (accTypeData.length > 0) {
      setTimeout(function () {
        res
          .status(200)
          .json({ status: 200, data: { historyData: accTypeData } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var apps = dcodeInfo.apps[0];
    const { id, min, max } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE prefund_dashboard SET min=$1, max=$2 where id=$3",
    //   [min, max, id]
    // );
    const resp = await conn("prefund_dashboard").where("id", id).update({
      min: min,
      max: max,
    });
    console.log(resp);
    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/external/balanceinquiry", (req, res, next) => {
  console.log(">>>>>>>>>>>>>>>>>>>>>>>>>/external/balanceInquiry");

  var requestBody = req.body;
  const apiserver2 = "http://10.11.100.116:9008";
  console.log(req.body);

  try {
    axios
      .post(apiserver2 + "/komi/api/v1/adapter/balanceinquiry", requestBody)
      .then((response) => res.json(response.data))
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getMonitoringUsed", async (req, res, next) => {
  // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>getMonitoringUsed");

  const apiserver2 = "http://10.11.100.116:9007";
  try {
    axios
      .get(apiserver2 + "/api/monitoring/getMonitoringUsed")
      .then((response) => res.json(response.data))
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getMonitoringPortal", async (req, res, next) => {
  // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>getMonitoring");
  try {
    // cpu
    var cpu = osu.cpu;
    var mem = osu.mem;
    var drive = osu.drive;
    cpu.usage().then((infoCpu) => {
      // console.log("cpu : ", infoCpu);
      mem.info().then((infoMemo) => {
        // console.log("memo : ", infoMemo);
        drive.info().then((infoDrive) => {
          // console.log("drive : ", infoDrive);
          resp = [
            { processor: { used: infoCpu }, memory: infoMemo, disk: infoDrive },
          ];
          res.status(200).json({ status: 200, data: resp[0] });
        });
      });
    });
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getCheckService", async (req, res, next) => {
  console.log(">>>>>>>>>>>>>>>>>>>>>>>>>checkService");

  const apiserver2 = "http://10.11.100.116:9007";
  try {
    axios
      .get(apiserver2 + "/api/monitoringApp/checkService")
      .then((response) => res.json(response.data))
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
  }
});
module.exports = router;
