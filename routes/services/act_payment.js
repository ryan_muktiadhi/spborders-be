const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
// var bodyParser = require('body-parser');
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();
const multer  = require('multer')
const upload = multer({ dest: './public/fileupload/' })
const fs = require('fs');

// router.use(bodyParser());
// router.use(bodyParser.json({limit: '50mb'}));
// router.use(bodyParser.urlencoded({limit: '50mb', extended: true}));



router.get("/getAllPays", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    var idtenant = dcodeInfo.idtenant;
    try {
   
      const payAdmins = await conn
        .select(
            "id", 
            "payount", 
            "payname", 
            "bankname", 
            "balance", 
            "created_by", 
            "created_at", 
            "updated_at", 
            "active", 
            "idapproval"
        )
        .from("rekeningbank")
        .orderBy("created_at", "desc");
  
      if (payAdmins.length > 0) {
        // setTimeout(function () {
          res.status(200).json({ status: 200, data: payAdmins });
        // }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: payAdmins });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  });

  router.post("/insertpay", async (req, res, next) => {
    var dcodeInfo = req.userData;
    const change_who = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;
    try {
      // var apps = dcodeInfo.apps[0];
      // SELECT id, payount, payname, bankname, balance, created_by, created_at, updated_at, active, idapproval
      const { payount, payname, bankname, balance } = req.body;
      // const resp = await pool.query(
      //   "INSERT INTO m_pay (bank_code, pay_code, bank_name, change_who, idtenant) VALUES ($1, $2, $3, $4, $5) ",
      //   [
      //     bank_code, pay_code, bank_name, dcodeInfo.id, dcodeInfo.idtenant
      //   ]
      // );
      const resp = await conn("rekeningbank")
        .insert({
          payount: payount,
          payname: payname,
          bankname: bankname,
          balance: balance,
          active: 3,
        })
        .returning(["payname"]);
      console.log(resp);
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_pay ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  
router.post("/uploadfile", upload.single('doccsv'), async (req, res, next) => {
var dcodeInfo = req.userData;
console.log("KIRIM FIIIIIIIIIIIIIIILE ",upload);
   try {
       
                // var apps = dcodeInfo.apps[0];
                // const { id, payname, payount, bankname, balance } = req.body;
                const today = moment().format("YYYY-MM-DD HH:mm:ss");
                console.log("req.header : ", req.headers);
                console.log("req.file : ", req.file);
                let origFile = req.file

                fs.readFile('./public/fileupload/' + origFile.filename, function(err, data) {
                    console.log('Data :>> ', data);
                    fs.writeFile("./public/fileupload/"+origFile.originalname, data, async function(err) {
                        if(err) {
                            return console.log("Error pas Nulis : ",err);
                        }
                        fs.unlink('./public/fileupload/' + origFile.filename, function(){
                            if(err) throw err;
                        });
                        console.log("The file was saved!");
                        // filename, iduser, created_by, created_at, updated_at, active, idapprovval, id
                        const resp = await conn("t_payment")
                          .insert({
                            filename: origFile.originalname,
                            iduser: dcodeInfo.id,
                            created_by: dcodeInfo.id,
                            active: 3,
                          })
                          .returning(["filename"]);
                        console.log(resp);
                        if (resp.length > 0) {
                          res.status(200).json({ status: 200, data: resp });
                        } else {
                          res.status(500).json({
                            status: 500,
                            data: "Error insert m_pay ",
                          });
                        }




                    });  
                });
                    // res.status(200).json({ status: 200, data: {} });
    } catch (err) {
        console.log(err);
        res.status(500).json({ status: 500, data: "Error insert User" });
    }
});



  router.post("/updatepay", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      // var apps = dcodeInfo.apps[0];
      const { id, payname, payount, bankname, balance } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      // payload = {
      //   id: this.payId,
      //   payname: this.groupForm.get('payname')?.value,
      //   payount: this.groupForm.get('payount')?.value,
      //   bankname: this.groupForm.get('bankname')?.value,
      //   balance: this.groupForm.get('balance')?.value,
      // };
      // const resp = await pool.query(
      //   "UPDATE m_pay SET bank_code=$1, pay_code=$2, bank_name=$3, last_update_date=$4 WHERE pay_code=$5 ",
      //   [
      //     bank_code, pay_code, bank_name, today, old_code
      //   ]
      // );
      const resp = await conn("rekeningbank").where("id", id).update({
        payount: payount,
        payname: payname,
        bankname: bankname,
        balance: balance,
        updated_at : today
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_pay ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  router.post("/updatepayapprove", async (req, res, next) => {
    var dcodeInfo = req.userData;
    try {
      const { id, oldactive, isactive, idapproval } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");
      const resp = await conn("rekeningbank").where("id", id).update({
        active:isactive,
        updated_at:today
      });
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_pay ",
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  });
  router.get("/deletepay/:id", async (req, res, next) => {
    // const authHeader = req.headers.authorization;
    console.log("################## >>>> ##################################");
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    let idpay = req.params.id;
    console.log("### Bank Code ### " + idpay);
    try {
      // let resp = await pool.query(
      //   "DELETE FROM m_pay where bank_code = $1",
      //   [bankcode]
      // );
      let resp = await conn("rekeningbank").where("id", idpay).del();
      res.status(200).json({ status: 200, data: "Success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
  });
  
  module.exports = router;