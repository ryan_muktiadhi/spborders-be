const express = require("express");
const knex = require("../../connection/dborm");

const router = express.Router();
const conn = knex.conn();

router.get("/test", async (req, res, next) => {
  console.log(conn);
  const users = await conn.select("*").from("t_transaction");
  console.log(users);
  res.json(users);
});

router.get("/getByid/:id", async (req, res, next) => {
  const users = await conn
    .where("id", req.params.id)
    .select("id", "name")
    .from("persons");
  res.json({ users });
});

router.post("/add", async (req, res, next) => {
  const users = await conn("persons")
    .returning(["id", "name"])
    .insert({ id: 77722, name: req.body.name });
  //.returning("*")
  //.toString();
  // const users = await conn("persons")
  //   .insert({ name: req.body.name })
  //   .then((row) => {
  //     console.log(row);
  //     return row[0];
  //   });
  res.json(users);
});

router.post("/update", async (req, res, next) => {
  const users = await conn("persons")
    .returning(["id"])
    .where("id", req.body.id)
    .update({ name: req.body.name });
  res.json(users);
});

router.post("/delete", async (req, res, next) => {
  const users = await conn("persons").where("id", req.body.id).del();
  res.json(users);
});

//getAllGroup
router.get("/getMonitoringUsed", async (req, res, next) => {
  //   const authHeader = req.headers.authorization;
  // var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    // console.log(
    //   ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    // );
    // const today = moment().format("YYYY-MM-DD");

    let resp = [
      {
        disk: {
          name: "/dev/mapper/vg_app-lv_app",
          size: "64G",
          used: "764M",
          available: "64G",
          percent: "90%",
        },
        processor: {
          used: 0.65,
        },
        memory: {
          totalMemory: "15.46Gb",
          usedMemory: "42.16%",
          availableMemory: "57.84%",
        },
      },
    ];
    if (resp.length > 0) {
      setTimeout(function () {
        res.status(200).json(resp[0]);
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
