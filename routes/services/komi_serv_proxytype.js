const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
router.use(checkAuth);

var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllProxyType", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const bicAdmins = await conn
      .where("idtenant", dcodeInfo.idtenant)
      .select(
        "id",
        "proxycode",
        "proxyname",
        "status",
        "created_date",

        "change_who",
        "last_update_date",
        "idtenant"
      )
      .from("m_proxytype");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.get("/getProxyType/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let bankcode = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    console.log(bankcode);
    const bicAdmins = await conn
      .where("id", bankcode)
      .select(
        "id",
        "proxycode",
        "proxyname",
        "status",
        "created_date",
        "change_who",
        "last_update_date",
        "idtenant"
      )
      .from("m_proxytype");
    console.log(bicAdmins);
    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insertProxyType", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { proxycode, proxyname, status } = req.body;

    const resp = await conn("m_proxytype")
      .returning(["proxycode", "proxyname", "status", "change_who", "idtenant"])
      .insert({
        proxycode: proxycode,
        proxyname: proxyname,
        status: status,
        change_who: dcodeInfo.id,
        idtenant: dcodeInfo.idtenant,
      });

    if (resp.length > 0) {
      console.log(resp);
      res.status(200).json({ status: 200, data: resp[0] });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_proxytype ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert m_proxytypes" });
  }
});

router.post("/updateProxyType", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { id, proxycode, proxyname, status } = req.body;
    console.log(req.body);
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("m_proxytype")
      .returning(["id", "proxycode", "proxyname", "status", "last_update_date"])
      .where("id", id)
      .update({
        proxycode: proxycode,
        proxyname: proxyname,
        status: status,
        last_update_date: conn.fn.now(),
      });
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp.rows });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error update m_proxytype",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/deleteProxyType/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let recid = req.params.id;
  // console.log("### Bank Code ### "+recid);
  try {
    let resp = await conn("m_proxytype").where("id", recid).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
