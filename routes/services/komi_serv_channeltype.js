const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllChannelType", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  const idtenant = dcodeInfo.idtenant;
  // const idtenant = 1;
  try {
    // console.log(
    //   ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    // );
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    //   var query = "SELECT id, paramname, paramvalua, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM m_systemparam WHERE idtenant=$1 order by created_date desc";

    // var query =
    //   "SELECT id, channelcode, channeltype, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM m_channeltype WHERE idtenant=$1 order by created_date desc";
    // const bicAdmins = await pool.query(query, [dcodeInfo.idtenant]);

    const bicAdmins = await conn
      .select(
        "id",
        "channelcode",
        "channeltype",
        "status",
        "created_date",
        "change_who",
        "last_update_date",
        "idtenant"
      )
      .from("m_channeltype")
      .where("idtenant", idtenant)
      .orderBy("created_date", "desc");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getChannelType/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let bankcode = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')

    // var query =
    //   "SELECT id,channelcode, channeltype, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM m_channeltype WHERE id=$1 order by created_date desc";
    // const bicAdmins = await pool.query(query, [bankcode]);

    const bicAdmins = await conn
      .select(
        "id",
        "channelcode",
        "channeltype",
        "status",
        "created_date",
        "change_who",
        "last_update_date",
        "idtenant"
      )
      .from("m_channeltype")
      .where("id", bankcode)
      .orderBy("created_date", "desc");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertChannelType", async (req, res, next) => {
  var dcodeInfo = req.userData;
  // const change_who = null;
  // const idtenant = 1;
  const change_who = dcodeInfo.id;
  const idtenant = dcodeInfo.idtenant;
  try {
    // var apps = dcodeInfo.apps[0];
    const { channelcode, channeltype, status } = req.body;
    // const resp = await pool.query(
    //   "INSERT INTO m_channeltype (channelcode, channeltype, status, change_who, idtenant) VALUES ($1, $2, $3, $4, $5) ",
    //   [channelcode, channeltype, status, dcodeInfo.id, dcodeInfo.idtenant]
    // );

    const resp = await conn("m_channeltype")
      .insert({
        channelcode: channelcode,
        channeltype: channeltype,
        status: status,
        change_who: change_who,
        idtenant: idtenant,
      })
      .returning(["id"]);
    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_ChannelType ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert m_ChannelTypes" });
  }
});

router.post("/updateChannelType", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var apps = dcodeInfo.apps[0];
    const { id, channelcode, channeltype, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE m_channeltype SET channeltype=$1, status=$2, last_update_date=$3, channelcode=$4 WHERE id=$5 ",
    //   [channeltype, status, today, channelcode, id]
    // );

    const resp = await conn("m_channeltype").where("id", id).update({
      channeltype: channeltype,
      status: status,
      last_update_date: today,
      channelcode: channelcode,
    });

    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error update m_channeltype",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.get("/deleteChannelType/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let recid = req.params.id;
  // console.log("### Bank Code ### "+recid);
  try {
    // let resp = await pool.query(
    //   "DELETE FROM m_channeltype where id = $1",
    //   [recid]
    // );
    let resp = await conn("m_channeltype").where("id", recid).del();

    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

router.get("/getAllConChannel", async (req, res, next) => {
  try {

    const data = await conn.select("*").from("kc_channel");
    console.log(data)
    if (data.length > 0) {
      setTimeout(function () {
        res
            .status(200)
            .json({status:200, data: { channel : data }});
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan"
        });
      }, 500);
    }
  } catch (e) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Error"
    });
  }
});

module.exports = router;
