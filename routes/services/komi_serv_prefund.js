const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
router.use(checkAuth);
var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllPrefund", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const prefunds = await conn
      .select(
        "idparticipant",
        "groupIdParticipant1.max_amount",
        "groupIdParticipant2.min_amount"
      )
      .from("m_prefund")
      .join(
        conn("m_prefund")
          .select("idparticipant")
          .max("amount as max_amount")
          .as("groupIdParticipant1"),
        "groupIdParticipant1.idparticipant",
        "m_prefund.idparticipant"
      )
      .join(
        conn("m_prefund")
          .select("idparticipant")
          .min("amount as min_amount")
          .as("groupIdParticipant2"),
        "groupIdParticipant2.idparticipant",
        "m_prefund.idparticipant"
      )
      .where("m_prefund.idtenant", dcodeInfo.idtenant)
      .orderBy("m_prefund.idparticipant", "desc");

    if (prefunds.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { prefunds: prefunds } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getPrefund/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let idparticipant = req.params.id;

    const prefunds = await conn
      .where({ idparticipant: idparticipant, idtenant: dcodeInfo.idtenant })
      .select("id", "idparticipant", "amount", "created_date")
      .from("m_prefund")
      .orderBy("created_date", "desc");

    if (prefunds.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { prefunds: prefunds[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insertPrefund", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { bank_code, bic_code, bank_name } = req.body;

    const resp = await conn("m_bic")
      .returning([
        "bank_code",
        "bic_code",
        "bank_name",
        "change_who",
        "idtenant",
      ])
      .insert({
        bank_code: bank_code,
        bic_code: bic_code,
        bank_name: bank_name,
        change_who: dcodeInfo.id,
        idtenant: dcodeInfo.idtenant,
      });

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/updatePrefund", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { old_code, bank_code, bic_code, bank_name } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const resp = await conn("m_prefund")
      .returning([
        "bank_code",
        "bic_code",
        "bank_name",
        "change_who",
        "idtenant",
      ])
      .where("bic_code", old_code)
      .update({
        bank_code: bank_code,
        bic_code: bic_code,
        bank_name: bank_name,
        last_update_date: today,
      });
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/deletePrefund/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let idparticipant = req.params.id;
  console.log("### Participant Id ### " + idparticipant);
  try {
    const resp = await conn("m_prefund")
      .where("idparticipant", idparticipant)
      .del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Prefund" });
  }
});

module.exports = router;
