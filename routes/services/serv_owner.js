const express = require("express"),app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");

/*POST END*/


router.get("/", async (req, res, next) => {
  try {
      const resp = await pool.query("Select * from public.cdid_owner order by id",[]);
      res.status(200).json({ status: 200, data: resp.rows });
  } catch (err) {
    
    res.status(500).json({ status: 500, data: "No data tenants found" });
  }
});



module.exports = router;
