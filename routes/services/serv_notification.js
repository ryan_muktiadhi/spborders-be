const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

router.get("/getallmodules", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    try {
      const allModules = await conn
        .select(
          "id", 
            "modulename", 
            "status",  
            "created_at", 
            "updated_at"
        )
        .from("spb_m_module");
      if (allModules.length > 0) {
        setTimeout(function () {
          res
            .status(200)
            .json({ status: 200, data: allModules });
        }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});
router.get("/getnotifbytenant", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    // var apps = dcodeInfo.apps[0];
    // var idtenant = dcodeInfo.idtenant;
    console.log(">>>>>>>>>>>>>>> ID TENANT ", dcodeInfo);
    try {
      const allNotify = await conn
        .select(
            "sn.id",
            "sn.idmodule",
            "sn.sbjmessage",
            "sn.receiveddate",
            "sn.usersenderid",
            "sn.userreceiveid",
            "sn.tenantsenderid",
            "sn.tenantreceiveid",
            "sn.status",
            "sn.created_by",
            "sn.created_at",
            "sn.updated_at",
            "smm.modulename"
        )
        .from("spb_notification as sn").innerJoin('spb_m_module as smm', 'sn.idmodule', '=', 'smm.id')
        .orderBy("created_at", "desc");
  
      if (allNotify.length > 0) {
        // setTimeout(function () {
          res.status(200).json({ status: 200, data: allNotify });
        // }, 500);
      } else {
        setTimeout(function () {
          res.status(200).json({ status: 202, data: allNotify });
        }, 500);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});

 
  
  module.exports = router;