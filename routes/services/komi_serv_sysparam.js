const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
//const conn = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const axios = require("axios");
const systemParam = require("./utils/getSystemParam");
const knex = require("../../connection/dborm");
router.use(checkAuth);
var conf = require("../../config.json");
const conn = knex.conn();
const apiserver = conf.kktserver;

//getAllGroup
router.get("/getAllSysParam", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    // console.log(
    //   ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    // );

    // var query =
    //   "SELECT id, paramname, paramvalua, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM public.m_systemparam WHERE idtenant=$1 order by created_date desc";
    // const bicAdmins = await conn.query(query, [dcodeInfo.idtenant]);

    const bicAdmins = await conn
      .where("id_tenant", dcodeInfo.idtenant)
      .select(
        "id",
        "paramname",
        "paramvalua",
        "status as active",
        "created_date",
        "change_who",
        "last_update_date",
        "id_tenant",
        "idapproval"
      )
      .from("m_systemparam");
    // console.log(bicAdmins);
    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.get("/getSysParam/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let bankcode = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')

    const bicAdmins = await conn
      .where("id", bankcode)
      .select(
        "id",
        "paramname",
        "paramvalua",
        "status",
        "created_date",
        "change_who",
        "last_update_date",
        "id_tenant",
        "idapproval"
      )
      .from("m_systemparam");
    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insertSysParam", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { paramname, paramvalua, status } = req.body;

    if (apps.withapproval > 0) {
      const resp = await conn("m_systemparam")
        .returning([
          "paramname",
          "paramvalua",
          "status",
          "change_who",
          "id_tenant",
        ])
        .insert({
          paramname: paramname,
          paramvalua: paramvalua,
          status: 3,
          change_who: dcodeInfo.id,
          id_tenant: dcodeInfo.idtenant,
        });
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_sysparams ",
        });
      }
    } else {
      const resp = await conn("m_systemparam")
        .returning([
          "paramname",
          "paramvalua",
          "status",
          "change_who",
          "id_tenant",
        ])
        .insert({
          paramname: paramname,
          paramvalua: paramvalua,
          status: status,
          change_who: dcodeInfo.id,
          id_tenant: dcodeInfo.idtenant,
        });

      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_sysparams ",
        });
      }
    }

    // const resp = await conn("m_systemparam")
    //   .returning([
    //     "paramname",
    //     "paramvalua",
    //     "status",
    //     "change_who",
    //     "id_tenant",
    //   ])
    //   .insert({
    //     paramname: paramname,
    //     paramvalua: paramvalua,
    //     status: status,
    //     change_who: dcodeInfo.id,
    //     id_tenant: dcodeInfo.idtenant,
    //   });

    // if (resp.length > 0) {
    //   res.status(200).json({ status: 200, data: resp });
    // } else {
    //   res.status(500).json({
    //     status: 500,
    //     data: "Error insert m_sysparams ",
    //   });
    // }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert m_sysparams" });
  }
});

router.post("/updateSysParam", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const token = req.headers.authorization;
  try {
    var apps = dcodeInfo.apps[0];
    const { id, paramname, paramvalua, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    if (apps.withapproval > 0) {
      console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + apps.withapproval);
      // console.log("LL " +token);
      axios
        .post(
          apiserver + "/api/log/setapprovaldata",
          {
            idapp: apps.id,
            idtenant: dcodeInfo.idtenant,
            cdmenu: "SSYPRM",
            jsondata: JSON.stringify(req.body),
            status: 0,
            typedata: status == 1 ? 7 : 5,
          },
          {
            headers: { Authorization: token },
          }
        )
        .then(async (resp) => {
          // console.log(JSON.stringify(resp.data));
          var userObj = resp.data.data;
          console.log(JSON.stringify(userObj));
          let idreturn = userObj[0].id;
          console.log("IIIIIIID RETURN " + idreturn);
          const respUpdate = await conn("m_systemparam")
            .returning([
              "paramname",
              "paramvalua",
              "status",
              "last_update_date",
            ])
            .where("id", id)
            .update({
              status: status == 1 ? 7 : 5,
              idapproval: idreturn,
            });
          if (respUpdate.length > 0) {
            res.status(200).json({ status: 200, data: respUpdate[0] });
          } else {
            res.status(500).json({
              status: 500,
              data: "Error insert m_systemparam ",
            });
          }
          // return;
        })
        .catch((err) => {
          console.log(err);
          return;
        });
    } else {
      const resp = await conn("m_systemparam")
        .returning(["paramname", "paramvalua", "status", "last_update_date"])
        .where("id", id)
        .update({
          paramname: paramname,
          paramvalua: paramvalua,
          status: status,
          last_update_date: today,
        });
      if (resp.length > 0) {
        res.status(200).json({ status: 200, data: resp[0] });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_systemparam",
        });
      }
    }

    // const resp = await conn("m_systemparam")
    //   .returning(["paramname", "paramvalua", "status", "last_update_date"])
    //   .where("id", id)
    //   .update({
    //     paramname: paramname,
    //     paramvalua: paramvalua,
    //     status: status,
    //     last_update_date: today,
    //   });

    // if (resp.length > 0) {
    //   res.status(200).json({ status: 200, data: resp[0] });
    // } else {
    //   res.status(500).json({
    //     status: 500,
    //     data: "Error insert m_bic ",
    //   });
    // }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/updateSysParamActive", async (req, res, next) => {
  const token = req.headers.authorization;
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { id, oldactive, isactive, idapproval } = req.body;
    // const today = moment().format("YYYY-MM-DD HH:mm:ss");
    let today = new Date();
    today.setHours(today.getHours() + 7);
    switch (oldactive) {
      case 3:
        // code block
        const resp = await conn("m_systemparam")
          .returning(["id", "last_update_date"])
          .where("id", id)
          .update({
            status: isactive,
            last_update_date: today,
          });
        if (resp.length > 0) {
          var userObj = resp[0];
          //console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
          res.status(200).json({ status: 200, data: userObj });
        } else {
          res.status(500).json({ status: 500, data: "Error update User" });
        }

        break;
      case 5:
      case 7:
        console.log(
          ">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS id " + idapproval
        );
        axios
          .post(
            apiserver + "/api/log/getapprovaleditdata",
            {
              iddata: idapproval,
            },
            {
              headers: { Authorization: token },
            }
          )
          .then(async (resp) => {
            // console.log(resp.data);
            let dataresp = resp.data.data;
            let objResp = JSON.parse(dataresp[0].jsondata);
            // '{"paramname":"PARAM_DATA_2","paramvalua":"2001","status":0,"id":"4"}'
            // console.log(objResp);
            // let datajson = JSON.parse(resp);
            // console.log(datajson.paramname);
            const respUpdateData = await conn("m_systemparam")
              .returning([
                "paramname",
                "paramvalua",
                "status",
                "last_update_date",
              ])
              .where("id", id)
              .update({
                paramname: objResp.paramname,
                paramvalua: objResp.paramvalua,
                status: objResp.status,
                last_update_date: today,
              });
            if (respUpdateData.length > 0) {
              res.status(200).json({ status: 200, data: respUpdateData[0] });
            } else {
              res.status(500).json({
                status: 500,
                data: "Error insert m_bic ",
              });
            }
            return;
          })
          .catch((err) => {
            console.log(err);
            return;
          });
        break;
      case 9:
        console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON SSSSSSSSSSSSSSSSSSSSS");
        let respDel;
        let respAppDelTmp = await conn
          .select("jsondata")
          .from("trx_approvaltmp")
          .where({ id: idapproval, status: 0 });

        break;
      default:
      // code block
    }

    // const resp = await conn("m_systemparam")
    //   .returning(["paramname", "paramvalua", "status", "last_update_date"])
    //   .where("id", id)
    //   .update({
    //     paramname: paramname,
    //     paramvalua: paramvalua,
    //     status: status,
    //     last_update_date: today,
    //   });

    // if (resp.length > 0) {
    //   res.status(200).json({ status: 200, data: resp[0] });
    // } else {
    //   res.status(500).json({
    //     status: 500,
    //     data: "Error insert m_bic ",
    //   });
    // }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/reject", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var toknid = req.tokenID;
  var apps = dcodeInfo.apps[0];
  var userObj;
  // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
  var levetid = parseInt(dcodeInfo.leveltenant);
  if (levetid == 0) {
    levetid = levetid + 1;
  }
  try {
    const { id, oldactive, isactive, idapproval } = req.body;
    let now = new Date();
    now.setHours(now.getHours() + 7);
    console.log(">>>>>>>>>>>>>>>>>>>>>>>.JSON");
    console.log(oldactive);
    let result;
    switch (oldactive) {
      case 3:
        result = await conn("m_systemparam").where({ id: id }).del();
        if (result) {
          res.status(200).json({ status: 200, data: "" });
        } else {
          res.status(422).json({ status: 422, data: "Unprocessable data" });
        }

        break;
      case 4:
        break;

      case (5, 7):
        result = await conn("m_systemparam").where({ id: id }).update({
          status: 0,
        });
        if (result) {
          res.status(200).json({ status: 200, data: "" });
        } else {
          res.status(422).json({ status: 422, data: "Unprocessable data" });
        }

        break;

        break;
      case 9:
        break;
      default:
      // code block
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "Error update User" });
  }
});

router.get("/deleteSysParam/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let recid = req.params.id;
  // console.log("### Bank Code ### "+recid);
  try {
    let resp = await conn("m_systemparam").where("id", recid).del();

    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
