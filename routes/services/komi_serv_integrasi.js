const express = require("express"),
    app = express();
const router = express.Router();
const axios = require("axios");
const conf = require("../../config.json");
const apiserver = conf.kktserver;



router.post("/emailnotification", (req, res, next) => {
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>/emailnotification");

    var requestBody = req.body;

    console.log(req.body);

    try {
        axios
            .post(apiserver + "/adm/send/sendemailnotification", requestBody)
            .then((response) => res.json(response.data))
            .catch((err) => res.send(err));
    } catch (err) {
        console.error("error", err);
    }
});

module.exports = router;
