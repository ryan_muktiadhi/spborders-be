const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllbic", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  var idtenant = dcodeInfo.idtenant;
  try {
    // console.log(">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query = "SELECT bank_code id, bank_code, bic_code, bank_name, last_update_date, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, idtenant FROM m_bic WHERE idtenant=$1 order by created_date desc";
    // const bicAdmins = await pool.query(
    //   query, [dcodeInfo.idtenant]);

    const bicAdmins = await conn
      .select(
        "bank_code as id",
        "bank_code",
        "bic_code",
        "bank_name",
        "last_update_date",
        "created_date",
        "change_who"
      )
      .where("idtenant", idtenant)
      .from("m_bic")
      .orderBy("created_date", "desc");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { bicAdmins: bicAdmins } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.get("/getBic/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let bankcode = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')

    // var query = "SELECT  bank_code id, bank_code, bic_code, bank_name, last_update_date, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, idtenant FROM m_bic WHERE bank_code=$1 order by created_date desc";
    // const bicAdmins = await pool.query(
    //   query, [bankcode]);

    const bicAdmins = await conn
      .select(
        "bank_code as id",
        "bank_code",
        "bic_code",
        "bank_name",
        "last_update_date",
        "created_date",
        "change_who"
      )
      .from("m_bic")
      .where("bank_code", bankcode)
      .orderBy("created_date", "desc");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res
          .status(200)
          .json({ status: 200, data: { bicAdmins: bicAdmins[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insertBic", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const change_who = dcodeInfo.id;
  const idtenant = dcodeInfo.idtenant;
  try {
    // var apps = dcodeInfo.apps[0];
    const { bank_code, bic_code, bank_name } = req.body;
    // const resp = await pool.query(
    //   "INSERT INTO m_bic (bank_code, bic_code, bank_name, change_who, idtenant) VALUES ($1, $2, $3, $4, $5) ",
    //   [
    //     bank_code, bic_code, bank_name, dcodeInfo.id, dcodeInfo.idtenant
    //   ]
    // );
    const resp = await conn("m_bic")
      .insert({
        bank_code: bank_code,
        bic_code: bic_code,
        bank_name: bank_name,
        change_who: change_who,
        idtenant: idtenant,
      })
      .returning(["bic_code"]);
    console.log(resp);
    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/updateBic", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var apps = dcodeInfo.apps[0];
    const { old_code, bank_code, bic_code, bank_name } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE m_bic SET bank_code=$1, bic_code=$2, bank_name=$3, last_update_date=$4 WHERE bic_code=$5 ",
    //   [
    //     bank_code, bic_code, bank_name, today, old_code
    //   ]
    // );
    const resp = await conn("m_bic").where("bic_code", old_code).update({
      bank_code: bank_code,
      bic_code: bic_code,
      bank_name: bank_name,
      last_update_date: today,
    });
    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/deleteBic/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let bankcode = req.params.id;
  console.log("### Bank Code ### " + bankcode);
  try {
    // let resp = await pool.query(
    //   "DELETE FROM m_bic where bank_code = $1",
    //   [bankcode]
    // );
    let resp = await conn("m_bic").where("bank_code", bankcode).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
