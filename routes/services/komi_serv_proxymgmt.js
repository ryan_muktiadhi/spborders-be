const express = require("express"),
    app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
const moment = require("moment");
const conn = knex.conn();
router.use(checkAuth);

//getAllTransaction
router.get("/getAll", async (req, res, next) => {
    var dcodeInfo = req.userData;

    try {
        const data = await conn("t_proxy_mgmt as tpm")
            .join("kc_channel as kc", "tpm.channel", "=", "kc.channel_type")
            .select("tpm.*" ,
            "kc.channel_name as channel_name")
        console.log(data)
        if (data.length > 0) {
            setTimeout(function () {
                res
                    .status(200)
                    .json({status:200, data: { proxy : data }});
            }, 500);
        } else {
            setTimeout(function () {
                res.status(200).json({
                    status: 202,
                    data: "Data tidak ditemukan"
                });
            }, 500);
        }

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Internal Error"
        });
    }
});

router.get("/getBySecondId", async (req, res, next) => {
    try {
        const data = await conn
            .select("scnd_id_type as secondId")
            .from('t_proxy_mgmt')
            .groupBy("scnd_id_type");

        console.log(data)

        if (data.length > 0) {
            res.status(200).json({ status: 200, data });
        } else {
            res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }
        
    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
})


router.post("/getByParam", async (req, res, next) => {
    try {

        const { startDate, endDate, secondid, channel, proxyaddrestype } = req.body;

        const respByParam = await conn ("t_proxy_mgmt as tpm")
            .join("kc_channel as kc", "tpm.channel", "=", "kc.channel_type")
            .select("tpm.*" ,"kc.channel_name as channel_name")
            .modify(function (queryBuilder){
                if (startDate && endDate) {
                    queryBuilder.whereBetween("request_dt", [
                        startDate,
                        endDate,
                    ]);
                }
                if (channel) {
                    queryBuilder.where("channel", channel);
                }
                if (secondid) {
                    queryBuilder.where("scnd_id_type", secondid);
                }
                if (proxyaddrestype) {
                    queryBuilder.where("proxy_type", proxyaddrestype);
                }
            });
        if (respByParam.length > 0) {
            res.status(200).json({ status: 200, data: respByParam });
        } else {
            res.status(200).json({
                status: 202,
                data: "No Content",
            });
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
});


module.exports = router;



