const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllBranch", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  var idtenant = dcodeInfo.idtenant;
  try {
    // console.log(
    //   ">>>>>>>>>>>> Tenant, Level " + JSON.stringify(dcodeInfo.leveltenant)
    // );
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT id, citycode, cityname, branchcode, branchname, status FROM m_branch WHERE idtenant=$1 order by created_date desc";
    // const Branchs = await pool.query(query, [dcodeInfo.idtenant]);

    const Branchs = await conn
      .select(
        "id",
        "citycode",
        "cityname",
        "branchcode",
        "branchname",
        "status"
      )
      .where("idtenant", idtenant)
      .from("m_branch")
      .orderBy("created_date", "desc");

    if (Branchs.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { Branchs: Branchs } });
      }, 500);
    } else {
      setTimeout(function () {
        res
          .status(200)
          .json({ status: 202, data: "Data tidak branch ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getBranch/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let branchid = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT  id, citycode, cityname, branchcode, branchname, status FROM m_branch WHERE id=$1 order by created_date desc";
    // const Branchs = await pool.query(query, [branchid]);

    const Branchs = await conn
      .select(
        "id",
        "citycode",
        "cityname",
        "branchcode",
        "branchname",
        "status"
      )
      .from("m_branch")
      .where("id", branchid)
      .orderBy("created_date", "desc");

    if (Branchs.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { Branchs: Branchs[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/insertBranch", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const change_who = dcodeInfo.id;
  const idtenant = dcodeInfo.idtenant;
  try {
    // var apps = dcodeInfo.apps[0];
    const { citycode, cityname, branchcode, branchname, status } = req.body;
    // const resp = await pool.query(
    //   "INSERT INTO m_branch (citycode, cityname, branchcode, branchname, status, change_who, idtenant) VALUES ($1, $2, $3, $4, $5, $6, $7) ",
    //   [
    //     citycode,
    //     cityname,
    //     branchcode,
    //     branchname,
    //     status,
    //     dcodeInfo.id,
    //     dcodeInfo.idtenant,
    //   ]
    // );

    const resp = await conn("m_branch")
      .insert({
        citycode: citycode,
        cityname: cityname,
        branchcode: branchcode,
        branchname: branchname,
        status: status,
        change_who: change_who,
        idtenant: idtenant,
      })
      .returning(["id"]);

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_branch ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/updateBranch", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var apps = dcodeInfo.apps[0];
    const { id, citycode, cityname, branchcode, branchname, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE m_branch SET citycode=$1, cityname=$2, branchcode=$3, branchname=$4, last_update_date=$5 , status=$6 WHERE id=$7 ",
    //   [citycode, cityname, branchcode, branchname, today, status, id]
    // );

    const resp = await conn("m_branch").where("id", id).update({
      citycode: citycode,
      cityname: cityname,
      branchcode: branchcode,
      branchname: branchname,
      last_update_date: today,
      status: status,
    });

    console.log(resp);
    if (resp > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_branch ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/deleteBranch/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  let branchid = req.params.id;
  console.log("### Bank Code ### " + branchid);
  try {
    // let resp = await pool.query("DELETE FROM m_branch where id = $1", [
    //   branchid,
    // ]);
    let resp = await conn("m_branch").where("id", branchid).del();

    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
