const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
const moment = require("moment");
const { duration } = require("moment-timezone");
const conn = knex.conn();
//router.use(checkAuth);

//getAllTransaction
router.get("/getAllTransaction", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    const data = await conn.select("*").from("t_transaction");
    console.log(data);
    if (data.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { transaction: data } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Error",
    });
  }
});

//getByParam
/*
router.post("/getTransactionByParam", async (req, res, next) => {
    var dcodeInfo = req.userData;
    
    try {
        const { startDate, endDate, status, channel, trxtype, bifasttrxno } = req.body;

        const resp = await conn ("t_transaction")
            .select("*").from("t_transaction")
            .modify(function (queryBuilder) {
                if (startDate && endDate) {
                    queryBuilder.whereBetween("trx_initiation_date", [startDate, endDate]);
                }
                if (status) {
                    queryBuilder.where("trx_status_code", status);
                }
                if (channel) {
                    queryBuilder.where("channel_type", channel);
                }
                if (trxtype) {
                    queryBuilder.where("trx_type", trxtype);
                }
                if (bifasttrxno) {
                    queryBuilder.where("bifast_trx_no", bifasttrxno);
                }
            });

        if (resp.length > 0) {
            res.status(200).json({ status: 200, data: resp });
        } else {
            res.status(200).json({
                status: 202,
                data: "No Content",
            });
        }
    } catch (e) {
        console.log(err);
        res.status(500).json({ status: 500, data: "Error insert transaction" });
    }
});
*/

router.post("/getTransactionByParam", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    const {
      startDate,
      endDate,
      interStartDate,
      interEndDate,
      status,
      channel,
      trxtype,
      bifasttrxno,
    } = req.body;

    const resp = await conn("t_transaction")
      .select("*")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        // if (startDate && endDate) {
        //   queryBuilder.whereBetween("trx_initiation_date", [
        //     startDate,
        //     endDate,
        //   ]);
        // }
        if (startDate && endDate && !(startDate == endDate)) {
          console.log("MASUK FALSE");
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          console.log("MASUK TRUE");
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
        if (status) {
          queryBuilder.where("trx_status_code", status);
        }
        if (channel) {
          queryBuilder.where("channel_type", channel);
        }
        if (trxtype) {
          queryBuilder.where("trx_type", trxtype);
        }
        if (bifasttrxno) {
          queryBuilder.where("bifast_trx_no", bifasttrxno);
        }
      });

    const respDebitS = await conn("t_transaction")
      .sum("trx_amount as amountDebitS")
      .count("id as countRowDebitS")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        if (startDate && endDate && !(startDate == endDate)) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
        if (status) {
          queryBuilder.where("trx_status_code", status);
        }
        if (channel) {
          queryBuilder.where("channel_type", channel);
        }
        if (trxtype) {
          queryBuilder.where("trx_type", trxtype);
        }
        if (bifasttrxno) {
          queryBuilder.where("bifast_trx_no", bifasttrxno);
        }
      })
      .where("trx_status_code", "S")
      .andWhere("trx_type", "I");

    const respCreditS = await conn("t_transaction")
      .sum("trx_amount as amountCreditS")
      .count("id as countRowCreditS")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        if (startDate && endDate && !(startDate == endDate)) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
        if (status) {
          queryBuilder.where("trx_status_code", status);
        }
        if (channel) {
          queryBuilder.where("channel_type", channel);
        }
        if (trxtype) {
          queryBuilder.where("trx_type", trxtype);
        }
        if (bifasttrxno) {
          queryBuilder.where("bifast_trx_no", bifasttrxno);
        }
      })
      .where("trx_status_code", "S")
      .andWhere("trx_type", "O");

    const respDebitE = await conn("t_transaction")
      .sum("trx_amount as amountDebitE")
      .count("id as countRowDebitE")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        if (startDate && endDate && !(startDate == endDate)) {
          console.log("MASUK FALSE");
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          console.log("MASUK TRUE");
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
        if (status) {
          queryBuilder.where("trx_status_code", status);
        }
        if (channel) {
          queryBuilder.where("channel_type", channel);
        }
        if (trxtype) {
          queryBuilder.where("trx_type", trxtype);
        }
        if (bifasttrxno) {
          queryBuilder.where("bifast_trx_no", bifasttrxno);
        }
      })
      .where("trx_status_code", "E")
      .andWhere("trx_type", "I");

    const respCreditE = await conn("t_transaction")
      .sum("trx_amount as amountCreditE")
      .count("id as countRowCreditE")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        if (startDate && endDate && !(startDate == endDate)) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
        if (status) {
          queryBuilder.where("trx_status_code", status);
        }
        if (channel) {
          queryBuilder.where("channel_type", channel);
        }
        if (trxtype) {
          queryBuilder.where("trx_type", trxtype);
        }
        if (bifasttrxno) {
          queryBuilder.where("bifast_trx_no", bifasttrxno);
        }
      })
      .where("trx_status_code", "E")
      .andWhere("trx_type", "O");
    const totresp = [
      {
        results: resp,
        total: {
          respDebitS: respDebitS[0],
          respCreditS: respCreditS[0],
          respDebitE: respDebitE[0],
          respCreditE: respCreditE[0],
        },
      },
    ];

    // console.log("resp::", totresp);

    if (resp.length > 0) {
      // res.status(200).json({ status: 200, data: resp });
      res.status(200).json({ status: 200, data: totresp[0] });
    } else {
      res.status(200).json({
        status: 202,
        data: "No Content",
      });
    }
  } catch (e) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert transaction" });
  }
});

//insertTransaction
router.post("/insertTransaction", async (req, res, next) => {
  try {
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    const {
      komi_unique_id,
      bifast_trx_no,
      komi_trx_no,
      channel_type,
      branch_code,
      recipient_bank,
      sender_bank,
      recipient_account_no,
      recipient_proxy_type,
      recipient_proxy_alias,
      recipient_account_name,
      sender_account_no,
      sender_account_name,
      charge_type,
      trx_type,
      trx_amount,
      trx_fee,
      trx_initiation_date,
      trx_status_code,
      trx_status_message,
      trx_response_code,
      trx_proxy_flag,
      trx_SLA_flag,
      trx_duration,
      trx_complete_date,
      trx_info01,
      trx_info02,
      trx_info03,
      trx_info04,
      trx_info05,
    } = req.body;

    var d = new moment(today);
    var d_duration = d.add(trx_duration);
    const t_complete = moment(d_duration).format("YYYY-MM-DD HH:mm:ss");

    console.log(t_complete);

    const insertTransactionResp = await conn("t_transaction")
      .returning([
        "komi_unique_id",
        "bifast_trx_no",
        "komi_trx_no",
        "channel_type",
        "branch_code",
        "recipient_bank",
        "sender_bank",
        "recipient_account_no",
        "recipient_proxy_type",
        "recipient_proxy_alias",
        "recipient_account_name",
        "sender_account_no",
        "sender_account_name",
        "charge_type",
        "trx_type",
        "trx_amount",
        "trx_fee",
        "trx_initiation_date",
        "trx_status_code",
        "trx_status_message",
        "trx_response_code",
        "trx_proxy_flag",
        "trx_SLA_flag",
        "trx_duration",
        "trx_complete_date",
        "trx_info01",
        "trx_info02",
        "trx_info03",
        "trx_info04",
        "trx_info05",
      ])
      .insert({
        komi_unique_id: komi_unique_id,
        bifast_trx_no: bifast_trx_no,
        komi_trx_no: komi_trx_no,
        channel_type: channel_type,
        branch_code: branch_code,
        recipient_bank: recipient_bank,
        sender_bank: sender_bank,
        recipient_account_no: recipient_account_no,
        recipient_proxy_type: recipient_proxy_type,
        recipient_proxy_alias: recipient_proxy_alias,
        recipient_account_name: recipient_account_name,
        sender_account_no: sender_account_no,
        sender_account_name: sender_account_name,
        charge_type: charge_type,
        trx_type: trx_type,
        trx_amount: trx_amount,
        trx_fee: trx_fee,
        trx_initiation_date: today,
        trx_status_code: trx_status_code,
        trx_status_message: trx_status_message,
        trx_response_code: trx_response_code,
        trx_proxy_flag: trx_proxy_flag,
        trx_SLA_flag: trx_SLA_flag,
        trx_duration: trx_duration,
        trx_complete_date: t_complete,
        trx_info01: trx_info01,
        trx_info02: trx_info02,
        trx_info03: trx_info03,
        trx_info04: trx_info04,
        trx_info05: trx_info05,
      });

    if (insertTransactionResp.length > 0) {
      res.status(201).json({
        status: 200,
        data: "Transaction inserted",
      });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert to table t_transaction",
      });
    }
  } catch (e) {
    console.log(e);
    res.status(500).json({ status: 500, data: "Error insert Data" });
  }
});

module.exports = router;
