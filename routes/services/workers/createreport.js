const { Pool, Client } = require("pg");
const connectionString =
  "postgressql://postgres:p@ssword.1@localhost:5432/kktkomidb";
const client = new Client({
  connectionString: connectionString,
});

const knex = require("../../../connection/dborm");
var conf = require("../../../config.json");
const conn = knex.conn();

const xl = require("excel4node");
const wb = new xl.Workbook();
const ws = wb.addWorksheet("Worksheet Name");
const moment = require("moment");

process.on("message", (msg) => {
  // console.log('Message from Parent', msg);

  if (msg) {
    conn("trx_log as tl")
      .join("m_channeltype as mct", "tl.channel", "=", "mct.channelcode ")
      .select(
        "tl.id",
        "tl.status",
        "tl.trxnumber",
        " tl.refnumbifast",
        "mct.channeltype as channel",
        "tl.destbank",
        "tl.srcbank",
        "tl.destaccnum",
        "tl.srcaccnum",
        "tl.destaccname",
        "tl.srcaccname",
        "tl.created_date",
        "tl.change_who",
        "tl.last_updated_date",
        "tl.amount",
        conn.raw(
          "CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype"
        ),
        "tl.idtenant"
      )
      .where({
        "tl.idtenant": msg.idtenant,
      })
      .modify(function (queryBuilder) {
        if (msg.start_date && msg.end_date) {
          queryBuilder.whereBetween("tl.created_date", [
            msg.start_date,
            msg.end_date,
          ]);
        }

        if (msg.trxtype) {
          if (msg.trxtype == 1) {
            queryBuilder.where("tl.transacttype", trxtype);
          }
        }
      })
      .then((resp) => {
        const headingColumnNames = [
          "Status",
          "Trx Number",
          "Reff BiFast",
          "channel",
          "destbank",
          "srcbank",
          "destaccnum",
          "srcaccnum",
          "destaccname",
          "srcaccname",
          "created_date",
          "change_who",
          "amount",
          "transacttype",
          "idtenant",
        ];
        //Write Column Title in Excel file
        let headingColumnIndex = 1;
        headingColumnNames.forEach((heading) => {
          ws.cell(1, headingColumnIndex++).string(heading);
        });
        const dataFrom = res.rows;
        // client.end();
        // console.log(dataFrom);
        //Write Data in Excel file
        let rowIndex = 2;
        dataFrom.forEach((record) => {
          ws.cell(rowIndex, 1).string(record.status);
          ws.cell(rowIndex, 2).string(record.trxnumber);
          ws.cell(rowIndex, 3).string(record.refnumbifast);
          ws.cell(rowIndex, 4).string(record.channel);
          ws.cell(rowIndex, 5).string(record.destbank);
          ws.cell(rowIndex, 6).string(record.srcbank);
          ws.cell(rowIndex, 7).string(record.destaccnum);
          ws.cell(rowIndex, 8).string(record.srcaccnum);
          ws.cell(rowIndex, 9).string(record.destaccname);
          ws.cell(rowIndex, 10).string(record.srcaccname);
          ws.cell(rowIndex, 11).string(record.created_date);
          ws.cell(rowIndex, 12).string(record.change_who);
          ws.cell(rowIndex, 13).string(record.amount + "");
          ws.cell(rowIndex, 14).string(record.transacttype);
          ws.cell(rowIndex, 15).string(record.idtenant + "");
          rowIndex++;
        });
        const today = moment().format("YYYYMMDDHHmmss");
        const todayUdate = moment().format("YYYY-MM-DD HH:mm:ss");
        wb.write("./public/reports/xls" + today + ".xlsx");
        // process.exit();
        setTimeout(function () {
          let path = `xls${today}.xlsx`;
          conn("trx_reportlog")
            .where("id", msg.iddata)
            .update({
              pathfile: path,
              last_update_date: todayUdate,
              status: 1,
            })
            .then((resp) => {
              console.log("Exiting CHild");
              client.end();
              process.exit();
            });
        }, 10000);
      })
      .catch((err) => {
        client.end();
        process.exit();
      });
  } else {
    process.exit();
  }
  //   if (msg) {
  //     client.connect();

  //     // Message from Parent { start_date: '2021-09-24',
  //     // end_date: '2021-09-30',
  //     // trxtype: 1,
  //     // desc: 'Testing Incoming 2',
  //     // iddata: '12' }
  //     let queryhelper = "";
  //     if (msg.start_date && msg.end_date)
  //       queryhelper =
  //         queryhelper +
  //         " and tl.created_date between '" +
  //         msg.start_date +
  //         "' and '" +
  //         msg.end_date +
  //         "'";
  //     if (msg.trxtype)
  //       queryhelper = queryhelper + " and tl.transacttype =" + msg.trxtype;

  //     //   console.log(">> Tambahan >> : "+queryhelper);

  //     // console.log("SELECT tl.id, tl.status, tl.trxnumber, tl.refnumbifast, mct.channeltype channel, tl.destbank, tl.srcbank, tl.destaccnum, tl.srcaccnum, tl.destaccname, tl.srcaccname, TO_CHAR(tl.created_date,'yyyy-MM-dd HH:mm:ss') created_date, tl.change_who, tl.last_updated_date, tl.amount, CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype, tl.idtenant FROM public.trx_log tl INNER JOIN m_channeltype mct ON tl.channel = mct.channelcode WHERE tl.idtenant="+msg.idtenant+" "+queryhelper+" order by tl.created_date desc")

  //     // client.query("SELECT tl.id, tl.status, tl.trxnumber, tl.refnumbifast, mct.channeltype channel, tl.destbank, tl.srcbank, tl.destaccnum, tl.srcaccnum, tl.destaccname, tl.srcaccname, TO_CHAR(tl.created_date,'yyyy-MM-dd HH:mm:ss') created_date, tl.change_who, tl.last_updated_date, tl.amount, CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype, tl.idtenant FROM public.trx_log tl INNER JOIN m_channeltype mct ON tl.channel = mct.channelcode WHERE tl.idtenant="+msg.idtenant+" "+queryhelper+" order by tl.created_date desc", (err, res)=>{

  //     client.query(
  //       "SELECT tl.id, tl.status, tl.trxnumber, tl.refnumbifast, mct.channeltype channel, tl.destbank, tl.srcbank, tl.destaccnum, tl.srcaccnum, tl.destaccname, tl.srcaccname, TO_CHAR(tl.created_date,'yyyy-MM-dd HH:mm:ss') created_date, tl.change_who, tl.last_updated_date, tl.amount, CASE WHEN tl.transacttype = 1 THEN 'Incoming' ELSE 'Outgoing' END  transacttype, tl.idtenant FROM public.trx_log tl INNER JOIN m_channeltype mct ON tl.channel = mct.channelcode WHERE tl.idtenant=" +
  //         msg.idtenant +
  //         " " +
  //         queryhelper +
  //         " order by tl.created_date desc",
  //       (err, res) => {
  //         // console.log(err, res)
  //         if (err == null) {
  //           const headingColumnNames = [
  //             "Status",
  //             "Trx Number",
  //             "Reff BiFast",
  //             "channel",
  //             "destbank",
  //             "srcbank",
  //             "destaccnum",
  //             "srcaccnum",
  //             "destaccname",
  //             "srcaccname",
  //             "created_date",
  //             "change_who",
  //             "amount",
  //             "transacttype",
  //             "idtenant",
  //           ];
  //           //Write Column Title in Excel file
  //           let headingColumnIndex = 1;
  //           headingColumnNames.forEach((heading) => {
  //             ws.cell(1, headingColumnIndex++).string(heading);
  //           });
  //           const dataFrom = res.rows;
  //           // client.end();
  //           // console.log(dataFrom);
  //           //Write Data in Excel file
  //           let rowIndex = 2;
  //           dataFrom.forEach((record) => {
  //             ws.cell(rowIndex, 1).string(record.status);
  //             ws.cell(rowIndex, 2).string(record.trxnumber);
  //             ws.cell(rowIndex, 3).string(record.refnumbifast);
  //             ws.cell(rowIndex, 4).string(record.channel);
  //             ws.cell(rowIndex, 5).string(record.destbank);
  //             ws.cell(rowIndex, 6).string(record.srcbank);
  //             ws.cell(rowIndex, 7).string(record.destaccnum);
  //             ws.cell(rowIndex, 8).string(record.srcaccnum);
  //             ws.cell(rowIndex, 9).string(record.destaccname);
  //             ws.cell(rowIndex, 10).string(record.srcaccname);
  //             ws.cell(rowIndex, 11).string(record.created_date);
  //             ws.cell(rowIndex, 12).string(record.change_who);
  //             ws.cell(rowIndex, 13).string(record.amount + "");
  //             ws.cell(rowIndex, 14).string(record.transacttype);
  //             ws.cell(rowIndex, 15).string(record.idtenant + "");
  //             rowIndex++;
  //           });
  //           const today = moment().format("YYYYMMDDHHmmss");
  //           const todayUdate = moment().format("YYYY-MM-DD HH:mm:ss");
  //           wb.write("./public/reports/xls" + today + ".xlsx");
  //           // process.exit();
  //           setTimeout(function () {
  //             client.query(
  //               "UPDATE public.trx_reportlog SET status=1, pathfile='xls" +
  //                 today +
  //                 ".xlsx', last_update_date='" +
  //                 todayUdate +
  //                 "' WHERE id=" +
  //                 msg.iddata +
  //                 ";",
  //               (err, res) => {
  //                 // if(err == null) {

  //                 // }
  //                 // console.log("UPDATE public.trx_reportlog status=1, pathfile='xls"+today+".xlsx' last_update_date='"+todayUdate+"' WHERE id="+msg.iddata+" ;");
  //                 console.log("Exiting CHild");
  //                 client.end();
  //                 process.exit();
  //               }
  //             );
  //           }, 10000);
  //         } else {
  //           client.end();
  //           process.exit();
  //         }
  //         // client.end();
  //         // process.exit();
  //       }
  //     );

  //     // client.query('SELECT * FROM public.mst_module', (err, res)=>{
  //     //     // console.log(err, res)
  //     //     if(err == null) {
  //     //         console.log('===================')
  //     //         console.log('Connected with command : '+res.command)
  //     //         console.log('===================')
  //     //         console.log('Found Row Count : '+res.rowCount)
  //     //         console.log('===================')
  //     //         console.log(res.rows)
  //     //     }
  //     //     client.end()
  //     // });
  //   } else {
  //     process.exit();
  //   }

  // client.connect();
  // client.query('SELECT * FROM public.mst_module', (err, res)=>{
  //     // console.log(err, res)
  //     if(err == null) {
  //         console.log('===================')
  //         console.log('Connected with command : '+res.command)
  //         console.log('===================')
  //         console.log('Found Row Count : '+res.rowCount)
  //         console.log('===================')
  //         console.log(res.rows)
  //     }
  //     client.end()
  // });
});
// let counter = 0;

// setInterval(() => {
//     process.send({counter: counter++});
//     if(counter == 10){
//         console.log("############### CHILD TERMINATED ##############################")
//         process.exit();
//     }
// }, 3000);
