const express = require("express");
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");

const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//router.use(checkAuth);

//getTPSData
router.get("/getTPS", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_transaction")
      .select("id", "trx_initiation_date", "trx_status_code")
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        date: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        tranSuccess: 0,
        tranFailed: 0,
        tranTimeOut: 0,
        minutes: "",
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();
      minutes = ("0" + minutes).slice(-2);
      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });

        if (existMin > -1) {
          obj.date = item.trx_initiation_date;
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          obj.date = item.trx_initiation_date;
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        obj.date = item.trx_initiation_date;
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    rsArr.map((hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          mData.tranSuccess =
            dt.trx_status_code == "S"
              ? mData.tranSuccess + 1
              : mData.tranSuccess;
          mData.tranFailed =
            dt.trx_status_code == "E" ? mData.tranFailed + 1 : mData.tranFailed;
          mData.tranTimeOut =
            dt.trx_status_code == "T"
              ? mData.tranTimeOut + 1
              : mData.tranTimeOut;
        });
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getDaily
router.get("/getDaily", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_transaction")
      .select("id", "trx_initiation_date", "trx_status_code", "trx_SLA_flag")
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        minutes: "",
        tranSuccess: 0,
        tranSuccessNotSLA: 0,
        tranFailed: 0,
        tranFailedNotSLA: 0,
        tranTimeOut: 0,
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();

      minutes = minutes > 0 && minutes < 29 ? "00" : "30";

      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });
        if (existMin > -1) {
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    rsArr.map((hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          if (dt.trx_status_code == "S") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranSuccess = mData.tranSuccess + 1)
              : (mData.tranSuccessNotSLA = mData.tranSuccessNotSLA + 1);
          }
          if (dt.trx_status_code == "E") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranFailed = mData.tranFailed + 1)
              : (mData.tranFailedNotSLA = mData.tranFailedNotSLA + 1);
          }
          if (dt.trx_status_code == "T") {
            dt.trx_status_code == "T"
              ? (mData.tranTimeOut = mData.tranTimeOut + 1)
              : (mData.tranTimeOut = mData.tranTimeOut);
          }
        });
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getWeekly
router.get("/getWeekly", async (req, res, next) => {
  try {
    const dayName = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_transaction")
      .select("id", "trx_initiation_date", "trx_status_code", "trx_SLA_flag")
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        day: "",
        tranSuccess: 0,
        tranSuccessNotSLA: 0,
        tranFailed: 0,
        tranFailedNotSLA: 0,
        tranTimeOut: 0,
        dataDay: [],
      };
      var day = new Date(item.trx_initiation_date).getDay();
      day = ("0" + day).slice(-2);
      obj.day = day;
      const existHour = rsArr.findIndex((dt) => {
        return dt.day == day;
      });
      if (existHour > -1) {
        rsArr[existHour].dataDay.push(item);
      } else {
        obj.dataDay.push(item);
        rsArr.push(obj);
      }
    });

    rsArr.map((hData) => {
      let day = hData.day.slice();
      hData.day = dayName[day[1]];
      hData.tranTotal = hData.tranTotal + hData.dataDay.length;
      hData.maxTran =
        hData.maxTran > hData.tranTotal ? hData.maxTran : hData.tranTotal;
      hData.dataDay.map((dt) => {
        if (dt.trx_status_code == "S") {
          dt.trx_SLA_flag == "1"
            ? (hData.tranSuccess = hData.tranSuccess + 1)
            : (hData.tranSuccessNotSLA = hData.tranSuccessNotSLA + 1);
        }
        if (dt.trx_status_code == "E") {
          dt.trx_SLA_flag == "1"
            ? (hData.tranFailed = hData.tranFailed + 1)
            : (hData.tranFailedNotSLA = hData.tranFailedNotSLA + 1);
        }
        if (dt.trx_status_code == "T") {
          dt.trx_status_code == "T"
            ? (hData.tranTimeOut = hData.tranTimeOut + 1)
            : (hData.tranTimeOut = hData.tranTimeOut);
        }
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getMonthly
router.get("/getMonthly", async (req, res, next) => {
  try {
    const monthName = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_transaction")
      .select("id", "trx_initiation_date", "trx_status_code", "trx_SLA_flag")
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        month: "",
        tranSuccess: 0,
        tranSuccessNotSLA: 0,
        tranFailed: 0,
        tranFailedNotSLA: 0,
        tranTimeOut: 0,
        dataMonth: [],
      };
      var month = new Date(item.trx_initiation_date).getMonth();
      month = ("0" + month).slice(-2);
      obj.month = month;
      const existHour = rsArr.findIndex((dt) => {
        return dt.month == month;
      });
      if (existHour > -1) {
        rsArr[existHour].dataMonth.push(item);
      } else {
        obj.dataMonth.push(item);
        rsArr.push(obj);
      }
    });

    rsArr.map((hData) => {
      let month = hData.month.slice();
      hData.month = monthName[month[0] == 0 ? month[1] : hData.month];
      hData.tranTotal = hData.tranTotal + hData.dataMonth.length;
      hData.maxTran =
        hData.maxTran > hData.tranTotal ? hData.maxTran : hData.tranTotal;
      hData.dataMonth.map((dt) => {
        if (dt.trx_status_code == "S") {
          dt.trx_SLA_flag == "1"
            ? (hData.tranSuccess = hData.tranSuccess + 1)
            : (hData.tranSuccessNotSLA = hData.tranSuccessNotSLA + 1);
        }
        if (dt.trx_status_code == "E") {
          dt.trx_SLA_flag == "1"
            ? (hData.tranFailed = hData.tranFailed + 1)
            : (hData.tranFailedNotSLA = hData.tranFailedNotSLA + 1);
        }
        if (dt.trx_status_code == "T") {
          dt.trx_status_code == "T"
            ? (hData.tranTimeOut = hData.tranTimeOut + 1)
            : (hData.tranTimeOut = hData.tranTimeOut);
        }
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getDialyBusiness
router.get("/getDailyBusiness", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_response_code",
        "trx_status_code"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        minutes: "",
        tranSuccess: 0,
        tranSuccessNotSLA: 0,
        tranFailed: 0,
        tranFailedNotSLA: 0,
        tranTimeOut: 0,
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();

      minutes = minutes > 0 && minutes < 29 ? "00" : "30";

      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });
        if (existMin > -1) {
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    rsArr.map((hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          if (dt.trx_status_code == "S") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranSuccess = mData.tranSuccess + 1)
              : (mData.tranSuccessNotSLA = mData.tranSuccessNotSLA + 1);
          }
          if (dt.trx_status_code == "E") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranFailed = hData.tranFailed + 1)
              : (mData.tranFailedNotSLA = mData.tranFailedNotSLA + 1);
          }
          if (dt.trx_status_code == "T") {
            dt.trx_status_code == "T"
              ? (mData.tranTimeOut = mData.tranTimeOut + 1)
              : (mData.tranTimeOut = mData.tranTimeOut);
          }

          // mData.tranSuccess =
          //   dt.trx_response_code?.toUpperCase() == "ACCEPTED"
          //     ? mData.tranSuccess + 1
          //     : mData.tranSuccess;
          // mData.tranFailed =
          //   dt.trx_response_code?.toUpperCase() == "REJECTED"
          //     ? mData.tranFailed + 1
          //     : mData.tranFailed;
          // mData.tranTimeOut =
          //   dt.trx_response_code?.toUpperCase() == "TIMEOUT"
          //     ? mData.tranTimeOut + 1
          //     : mData.tranTimeOut;
        });
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
