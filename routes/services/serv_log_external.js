const express = require("express"),
    app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
const moment = require("moment");
const {duration} = require("moment-timezone");
const {where} = require("sequelize");
const conn = knex.conn();
const systemParam = require("./utils/getSystemParam");
const axios = require("axios");

router.post("/insert", async (req, res, next) => {

    try {

        const today = moment().format("YYYY-MM-DD HH:mm:ss");

        const { apimessage } = req.body
        const { codelog, data } = apimessage;
        let { komi_unique_id, bifast_trx_no, komi_trx_no, channel_type, trx_type, trx_initiation_date,
                    trx_duration, error_msg, sender_bank, recipient_bank, recipient_account_no, recipient_account_name,
                    sender_account_no, sender_account_name, proxyFlag, charge_type, trx_amount, status_code, response_code,
                    reason_code, reason_message, trx_info01, trx_info02, trx_info03, trx_info04,
                    trx_info05, branch_code, recipient_proxy_type, recipient_proxy_alias, trx_fee,
            scnd_id_type, scnd_id_value, call_status, proxy_type, proxy_alias, proxy_regn_opr, komi_trns_id,
            account_type, chnl_no_ref, customer_id, customer_type, resident_status, town_name, branch, trx_SLA_flag
        } = data;

        console.log(codelog);

        if (codelog == "CT" ) {
            var d = new moment(trx_initiation_date);
            var d_duration = d.add(trx_duration)
            const t_complete = moment(d_duration).format("YYYY-MM-DD HH:mm:ss.SSS");
            const amount = Math.trunc(trx_amount)
            const statuscd =  status_code.charAt(0)

            let param = await systemParam.getAllParam();
            let paramSLA = param.KOMI_CORE_SLA_CT;
            console.log(paramSLA);

            if ( parseInt(trx_duration) <=  parseInt(paramSLA)) {
                trx_SLA_flag = "1"
            } else {
                trx_SLA_flag = "2"
            }

            const  insertData = await conn("t_transaction")
                .insert({
                    komi_unique_id: komi_unique_id,
                    bifast_trx_no: bifast_trx_no,
                    komi_trx_no: komi_trx_no,
                    channel_type: channel_type,
                    branch_code: branch_code,
                    recipient_bank: recipient_bank,
                    sender_bank: sender_bank,
                    recipient_account_no: recipient_account_no,
                    recipient_proxy_type: recipient_proxy_type,
                    recipient_proxy_alias: recipient_proxy_alias,
                    recipient_account_name: recipient_account_name,
                    sender_account_no: sender_account_no,
                    sender_account_name: sender_account_name,
                    charge_type: charge_type,
                    trx_type: trx_type,
                    trx_amount: amount,
                    trx_fee: trx_fee,
                    trx_initiation_date: trx_initiation_date,
                    trx_status_code: statuscd,
                    trx_status_message: error_msg,
                    trx_response_code: response_code,
                    trx_reason_code: reason_code,
                    trx_reason_message: reason_message,
                    trx_proxy_flag: proxyFlag,
                    proxy_type: proxy_type,
                    trx_SLA_flag: trx_SLA_flag,
                    trx_duration: trx_duration,
                    trx_complete_date: t_complete,
                    trx_info01: trx_info01,
                    trx_info02: trx_info02,
                    trx_info03: trx_info03,
                    trx_info04: trx_info04,
                    trx_info05: trx_info05
                })
                .returning([
                    "komi_unique_id",
                    "trx_complete_date",
                    "trx_SLA_flag"
                ]);

            if (insertData.length > 0) {
                res.status(201).json({
                    status: 200,
                    data: "Data Inserted"
                });
                return;
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error insert to table t_transaction",
                });
                return;
            }
        }

        if (codelog == "AE") {
            var d = new moment(trx_initiation_date);
            var d_duration = d.add(trx_duration)
            const t_complete = moment(d_duration).format("YYYY-MM-DD HH:mm:ss.SSS");
            const amount = Math.trunc(trx_amount)
            const statuscd =  status_code.charAt(0)

            let param = await systemParam.getAllParam();
            let paramSLA = param.KOMI_CORE_SLA_AE;

            if ( parseInt(trx_duration) <=  parseInt(paramSLA)) {
                trx_SLA_flag = "1"
            } else {
                trx_SLA_flag = "2"
            }

            const  insertData = await conn("t_accountenquiry")
                .insert({
                    komi_unique_id: komi_unique_id,
                    bifast_trx_no: bifast_trx_no,
                    komi_trx_no: komi_trx_no,
                    channel_type: channel_type,
                    branch_code: branch_code,
                    recipient_bank: recipient_bank,
                    sender_bank: sender_bank,
                    recipient_account_no: recipient_account_no,
                    recipient_proxy_type: recipient_proxy_type,
                    recipient_proxy_alias: recipient_proxy_alias,
                    recipient_account_name: recipient_account_name,
                    sender_account_no: sender_account_no,
                    sender_account_name: sender_account_name,
                    trx_type: trx_type,
                    trx_initiation_date: trx_initiation_date,
                    trx_status_code: statuscd,
                    trx_status_message: error_msg,
                    trx_response_code: response_code,
                    trx_reason_code: reason_code,
                    trx_reason_message: reason_message,
                    trx_proxy_flag: proxyFlag,
                    proxy_type: proxy_type,
                    trx_SLA_flag: trx_SLA_flag,
                    trx_duration: trx_duration,
                    trx_complete_date: t_complete,
                    trx_info01: trx_info01,
                })
                .returning([
                    "komi_unique_id",
                    "trx_complete_date"
                ]);

            if (insertData.length > 0) {
                res.status(201).json({
                    status: 200,
                    data: "Data Inserted"
                });
                return;
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error insert to table",
                });
                return;
            }
        }

        if (codelog == "PS") {  //Update status

            let param = await systemParam.getAllParam();
            let paramSLA = param.KOMI_CORE_SLA_CT;
            console.log(paramSLA);

            if ( parseInt(trx_duration) <=  parseInt(paramSLA)) {
                trx_SLA_flag = "1"
            } else {
                trx_SLA_flag = "2"
            }

            const updateData = await conn("t_transaction")
                .where("bifast_trx_no", bifast_trx_no)
                .update({
                    bifast_trx_no: bifast_trx_no,
                    komi_unique_id: komi_unique_id,
                    trx_duration: trx_duration,
                    trx_status_code: status_code,
                    trx_response_code: response_code,
                    trx_reason_code: reason_code,
                    trx_reason_message: reason_message,
                    trx_SLA_flag: trx_SLA_flag
                });

            if (updateData > 0) {
                res.status(200).json({
                    status: 200,
                    data: "Update Data" });
                return;
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error update data ",
                });
                return;
            }
        } //update

        if (codelog == "PR") { //ProxyManagement

            const insertProxy = await conn("t_proxy_mgmt")
                .insert({
                    komi_unique_id: komi_unique_id,
                    komi_trx_no: komi_trx_no,
                    komi_trns_id: komi_trns_id,
                    bifast_trx_no: bifast_trx_no,
                    account_name: sender_account_name,
                    account_number: sender_account_no,
                    account_type: account_type,
                    call_status: call_status,
                    chnl_no_ref: chnl_no_ref,
                    cihub_elapsed_time: trx_duration,
                    customer_id: customer_id,
                    customer_type: customer_type,
                    display_name: sender_account_name,
                    operation_type: proxy_regn_opr,
                    proxy_type: proxy_type,
                    proxy_value: proxy_alias,
                    request_dt: trx_initiation_date,
                    resident_status: resident_status,
                    resp_status: response_code,
                    town_name: town_name,
                    branch: branch,
                    channel: channel_type,
                    scnd_value: scnd_id_value,
                    scnd_id_type: scnd_id_type,
                    trx_reason_code: reason_code,
                    trx_reason_message: reason_message,
                    trx_type: trx_type,
                    sender_bank: sender_bank
                })
                .returning([
                    "komi_unique_id",
                    "komi_trx_no",
                    "scnd_value",
                    "scnd_id_type"
                ]);

            if (insertProxy.length > 0) {
                res.status(200).json({
                    status: 200,
                    data: "Data Inserted" });
                return;
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error update data ",
                });
                return;
            }
        } else {
            res.status(500).json({
                status: 500,
                data: "codelog undefine ",
            });
            return;
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({ status: 500, data: "Error" });
    }
});


module.exports = router;



